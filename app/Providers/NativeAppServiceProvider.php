<?php

declare(strict_types=1);

namespace App\Providers;

use App\Livewire\Pages\LapPage;
use Native\Laravel\Facades\Menu;
use Native\Laravel\Facades\MenuBar;
use Native\Laravel\Facades\Window;
use Native\Laravel\Contracts\ProvidesPhpIni;

class NativeAppServiceProvider implements ProvidesPhpIni
{
    public function boot(): void
    {
        // Empty Menu
        Menu::create();

        Window::open('home')
            ->title('LA POM')
            ->route('home')
            ->width(360)
            ->height(200)
            ->resizable(false)
            ;
    }

    public function phpIni(): array
    {
        return [
        ];
    }
}
