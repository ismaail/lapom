<?php

declare(strict_types=1);

namespace Domain\Lap\Concerns;

enum LapType: int
{
    case Regular = 1;
    case Break = 2;
}
