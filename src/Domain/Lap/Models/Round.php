<?php

declare(strict_types=1);

namespace Domain\Lap\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Round extends Model
{
    protected $table = 'lap_rounds';

    protected $fillable = [
        'start_time',
        'end_time',
    ];

    public $timestamps = false;

    /**
     * @return array<string, string>
     */
    public function casts(): array
    {
        return [
            'start_time' => 'immutable_datetime',
            'end_time' => 'immutable_datetime',
        ];
    }

    public function lap(): BelongsTo
    {
        return $this->belongsTo(Lap::class, 'lap_id', 'id');
    }
}
