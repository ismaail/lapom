<?php

declare(strict_types=1);

namespace Domain\Lap\Models;

use Domain\Lap\Concerns\LapType;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Lap extends Model
{
    protected $table = 'laps';

    /**
     * @var list<string>
     */
    protected $fillable = [
        'type',
        'duration',
        'elapsed_time',
    ];

    /**
     * @var list<string|\class-string>
     */
    protected $casts = [
        'type' => LapType::class,
    ];

    public function rounds(): HasMany
    {
        return $this->hasMany(Round::class, 'lap_id', 'id');
    }
}
