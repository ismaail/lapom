<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    public function up(): void
    {
        Schema::create('lap_rounds', function (Blueprint $table) {
            $table->id();
            $table->foreignId('lap_id')->constrained('laps')->cascadeOnUpdate()->cascadeOnDelete();
            $table->timestamp('starts_at');
            $table->timestamp('ends_at')->nullable();
            $table->unsignedInteger('elapsed_time')->default(0);
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('lap_rounds');
    }
};
